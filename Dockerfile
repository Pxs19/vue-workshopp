FROM node:20-slim as build-stage

RUN npm install -g http-server

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

COPY nginx.conf /etc/nginx/conf.d/

EXPOSE 8080

CMD [ "http-server", "dist" ]

# WORKDIR /usr/src/app

# COPY package.json ./

# COPY . .

# RUN npm install

# RUN npm run build

# FROM nginx:stable-alpine as production-stage
# COPY --from=build-stage /usr/src/app/dist /usr/share/nginx/html
# EXPOSE 80
# CMD ["nginx", "-g", "daemon off;"]