import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#00000", // your primary color
        greyW: "#E0E0E0",
        // secondary: "#424242", // your secondary color
        // accent: "#82B1FF", // your accent color
        // maincl: "#00000",
      },
    },
  },
});
