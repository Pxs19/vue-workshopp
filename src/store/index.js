import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

// const BASE_URL = "https://jsonplaceholder.typicode.com";

export default new Vuex.Store({
  state: {
    isLogin: false,
    products: [],
    selectedProduct: {},
    carts: [],
    sumincarts: 0,
    userProducts: [],
  },
  getters: {
    getProducts: (state) => state.products,
    getLoginstate: (state) => state.isLogin,
    getSelectedProduct: (state) => state.selectedProduct,
    getCart: (state) => state.carts,
    getSummary: (state) => {
      return state.carts.reduce((total, item) => {
        return total + item.price * item.selectedAmount;
      }, 0);
    },
    getUserProducts: (state) => state.userProducts,
  },
  mutations: {
    SET_PRODUCTS(state, products) {
      state.products = products;
    },
    SET_LOGGIN(state, isLogin) {
      state.isLogin = isLogin;
    },
    SET_SELECTED_PRODUCT(state, selectedProduct) {
      state.selectedProduct = selectedProduct;
    },
    SET_CARTS(state, carts) {
      // for (const i = 0; i < carts.length; i++) {
      //   // if()
      // }

      state.carts.push(carts);
    },
    SET_BLANK_CART(state, carts) {
      state.carts = carts;
    },
    SET_USER_PRODUCTS(state, userProducts) {
      state.userProducts = userProducts;
    },
  },
  actions: {
    async fetchProducts({ commit }, token) {
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      try {
        const response = await axios.get(`http://localhost:3000/products`, config);
        commit("SET_PRODUCTS", response.data);
      } catch (error) {
        console.log(error);
      }
    },
    async fetchuserproducts({ commit }, token) {
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };

      try {
        const response = await axios.get(
          `http://localhost:3000/users/products`,
          config
        );

        commit("SET_USER_PRODUCTS", response.data);
      } catch (error) {
        console.log(error);
      }
    },
    async fetchSelectedProduct({ commit }, { id, token }) {
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      try {
        console.log("======================alkdflakodk");
        console.log(token);
        console.log(id);
        const response = await axios.get(
          "http://localhost:3000/products/" + id,
          config
        );
        commit("SET_SELECTED_PRODUCT", response.data);
      } catch (error) {
        console.log(error);
      }
    },
    async isloggin({ commit }) {
      try {
        // const response = await
      } catch (error) {
        console.log(error);
      }

      commit("SET_LOGGIN", true);
    },
    addtoCarts({ commit }, data) {
      commit("SET_CARTS", data);
    },
    removeCarts({ commit }, data) {
      commit("SET_BLANK_CART", data);
    },
  },
  modules: {},
});
