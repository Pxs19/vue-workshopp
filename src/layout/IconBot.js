const iconBotNav = [
  {
    icon: "mdi-home-outline",
    pathName: "home",
    name: "home",
  },
  {
    icon: "mdi-account-outline",
    pathName: "profile",
    name: "profile",
  },
  {
    icon: "mdi-calculator-variant",
    pathName: "grade",
    name: "grade",
  },
  {
    icon: "mdi-cog-outline",
    pathName: "signup",
    name: "signup",
  },
];

export default iconBotNav;
