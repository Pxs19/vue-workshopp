const iconRoute = [
  {
    icon: "mdi-home",
    pathName: "home",
    name: "home",
  },
  {
    icon: "mdi-cart",
    pathName: "carts",
    name: "carts",
  },
  {
    icon: "mdi-file-edit",
    pathName: "manage",
    name: "manage",
  },
  {
    icon: "mdi-history",
    pathName: "order",
    name: "order",
  },
  {
    icon: "mdi-account",
    pathName: "profile",
    name: "profile",
  },

  {
    icon: "mdi-calculator-variant",
    pathName: "grade",
    name: "grade",
  },
];

export default iconRoute;
