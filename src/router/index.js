import Vue from "vue";
import VueRouter from "vue-router";
import HomeView from "../views/HomeView.vue";
import ProfileView from "@/views/ProfileViews/ProfileView.vue";
import TeamView from "@/views/ProfileViews/FavoriteViews/TeamView.vue";
import SongView from "@/views/ProfileViews/FavoriteViews/SongView.vue";
import GameView from "@/views/ProfileViews/FavoriteViews/GameView.vue";
import AboutView from "../views/ProfileViews/AboutView.vue";
import GradeView from "@/views/GradeView.vue";
import SignupView from "@/views/auth/SignupView.vue";
import SettingView from "@/views/setting/SettingView.vue";
import SigninView from "@/views/auth/SigninView.vue";
import AuthLayout from "@/views/auth/AuthLayout.vue";
import ProductdetailView from "@/views/ProductdetailView.vue";
import CartView from "@/views/carts/CartView.vue";
import AdminView from "@/views/AdminView.vue";
import ManageproductView from "@/views/ManageproductView.vue";
import EditProductView from "@/views/EditProductView.vue";
import OrderView from "@/views/MyorderView.vue";

// import Me from "@/views/Me.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "",
    // name: "truelayout",
    component: () => import("@/views/LayoutView.vue"),
    children: [
      {
        path: "/",
        name: "home",
        component: HomeView,
      },
      {
        path: "/product/:id",
        name: "productdetail",
        component: ProductdetailView,
      },
      {
        path: "/manage/product/edit/:id",
        name: "editproduct",
        component: EditProductView,
      },
      {
        path: "/manage",
        name: "manage",
        component: ManageproductView,
      },
      {
        path: "/setting",
        name: "setting",
        component: SettingView,
      },
      {
        path: "/order",
        name: "order",
        component: OrderView,
      },
      {
        path: "/admin",
        name: "admin",
        component: AdminView,
      },
      {
        path: "/carts",
        name: "carts",
        component: CartView,
      },
      {
        path: "/profile",
        name: "profile",
        component: ProfileView,
        children: [
          {
            path: "",
            name: "about",
            component: AboutView,
          },
          {
            path: "team",
            name: "team",
            component: TeamView,
          },
          {
            path: "song",
            name: "song",
            component: SongView,
          },
          {
            path: "game",
            name: "game",
            component: GameView,
          },
        ],
      },
      {
        path: "/grade",
        name: "grade",
        component: GradeView,
      },
      {
        path: "/simple",
        name: "simple",
        component: () => import("@/views//Simple.vue"),
      },
    ],
  },
  // {
  //   path: "/about",
  //   name: "about",
  //   component: () => import("../views/AboutView.vue"),
  // },
  // {
  //   path: "",
  //   name: "toolbar",
  //   component: () => import("../views/Toolbar.vue"),
  //   children: [
  //     {
  //       path: "/me",
  //       name: "me",
  //       component: Me,
  //     },
  //   ],
  // },
  {
    path: "",
    name: "authlayout",
    component: AuthLayout,
    children: [
      {
        path: "/signup",
        name: "signup",
        component: SignupView,
      },
      {
        path: "/signin",
        name: "signin",
        component: SigninView,
      },
    ],
  },

  {
    path: "/apicon",
    name: "apicon",
    component: () => import("@/views/Apicon.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
